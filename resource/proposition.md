# list

## liste de systèmes possibles de gestion des interactions

### 1. POURCENTAGE

* système type jdr Warhammer, nombreuses possibilitées de compétences dépendant de pourçentage

* exemple : capacité de combat : 55, le joueur lance un D100 pour toucher et doit, pour réussir obtenir un résultat inférieur à ce chiffre.

* permet une grande variété de compétences faciliment gérable avec des D100, charisme, agilité, etc.

* je pense qu'il est important de coupler ce système avec des constantes type pv, force.

> remarque : nous devons trouver notre système de jeu, nous permettant d'avoir une certaine liberté pour créer en nous posant des contraintes structurantes (Whaou!).

### 2. SYSTÈME d20

* type d&d : jet d'un d20 plus les gains de carac du perso confronté aux caracs de l'adversaire ou de l'évènement, test d'opposition.

* sympa mais nécessite une grande maîtrise (je pense) dans la définition des seuils de réussites (peut être plus adapté pour des jeux multi...). à débattre. les effets peuvent aussi être gérés par des DD de taille différentes : d4, d6, d8, d12...

> système efficace qui a fait ses preuves depuis longtemps.

### 3. SYSTEME de seuil

* type shadowrun ou GoT, les caracs sont sous formes de points représentant le nombre de DD à lancer. Les DD sont onsidérés comme validés selon le seuil qu'ils ont atteints (4+ ou 5+ etc...), et c'est le nombre de réussite qui est confronté à la difficulté de l'action réalisé (3 ou 4 réussite par exemple).

> système cool et efficace mais que je maîtrise moins, à tester.

Je (PE) propose une variante du système de seuil sur 5 niveaux que j'avais utilisé il y a un bout de temps.

1. Profane
2. Novice
3. Disciple
4. Adepte
5. Maitre

## Bref

> N'hésitez pas à rajouter des systèmes ou envie, il s'agit d'un doc de base.
***
à voir pour le système de compétences : je pensais à des systèmes de point d'action
***
a vous de voir.